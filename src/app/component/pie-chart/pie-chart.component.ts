import {Component, Input, OnChanges, OnInit} from '@angular/core';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit, OnChanges {

  pieChartLabels = ['Like', 'Nope'];
  colors = [
    {
      backgroundColor: ['green', 'red'],
    },
  ];
  pieChartData: number[] = [];
  pieChartType = 'pie';
  @Input() nbLike: number;
  @Input() nbNope: number;

  constructor() {
  }

  ngOnInit() {
    this.pieChartData = [this.nbLike, this.nbNope];
  }

  ngOnChanges(): void {
    this.pieChartData = [this.nbLike, this.nbNope];
  }

}
