import {Component, OnInit} from '@angular/core';
import {ZinderService} from '../../service/zinder.service';
import {Profil} from '../../model/profil';
import {Interet} from '../../model/interet';

@Component({
  selector: 'app-match-page',
  templateUrl: './match-page.component.html',
  styleUrls: ['./match-page.component.css']
})
export class MatchPageComponent implements OnInit {

  profils: Profil[];
  interets: Interet[];
  filtreInteret: string;

  constructor(private zinderService: ZinderService) {
  }

  ngOnInit() {
    this.zinderService.getProfils().subscribe(profils => {
      this.profils = profils;
    });
    this.zinderService.getInterets().subscribe(interets => {
      this.interets = interets;
    });
  }

  getProfils(): Profil[] {
    if (!this.filtreInteret || this.filtreInteret === '') {
      return this.profils;
    }
    return this.profils.filter(profil => profil.interets.find(i => i === this.filtreInteret));
  }

}
