import {Component, OnInit} from '@angular/core';
import {ZinderService} from '../../service/zinder.service';
import {Interet} from '../../model/interet';
import {NewProfil} from '../../model/new-profil';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {
  nom: string;
  prenom: string;
  photoUrl: string;
  interets: Interet[];
  interetsSelected: string[] = [];

  constructor(private zinderService: ZinderService, private router: Router) {
  }

  ngOnInit() {
    this.zinderService.getInterets().subscribe(interets => this.interets = interets);
  }

  interetChange(id: string, $event: Event) {
    // @ts-ignore
    if ($event.target.checked) {
      this.interetsSelected.push(id);
    } else {
      this.interetsSelected = this.interetsSelected.filter(i => i !== id);
    }
  }

  valider() {
    const newProfil = new NewProfil();
    newProfil.nom = this.nom;
    newProfil.prenom = this.prenom;
    newProfil.photoUrl = this.photoUrl;
    newProfil.interets = this.interetsSelected;
    this.zinderService.createProfil(newProfil).subscribe(rep => this.router.navigate(['']));
  }
}
