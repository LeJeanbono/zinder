import {Component, OnInit} from '@angular/core';
import {ZinderService} from '../../service/zinder.service';
import {Match} from '../../model/match';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit {

  matchs: Match[] = [];

  constructor(private zinderService: ZinderService) {
  }

  ngOnInit() {
    this.zinderService.getMatchs().subscribe(matchs => {
      this.matchs = matchs;
    });
  }

  getNbMatchType(type: boolean): number {
    return this.matchs.filter(m => m.match === type).length;
  }

  deleteMatch(id: string) {
    this.zinderService.deleteMatch(id).subscribe(() => {
      this.zinderService.getMatchs().subscribe(matchs => {
        this.matchs = matchs;
      });
    });
  }
}
