import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Match} from '../../model/match';
import {ZinderService} from '../../service/zinder.service';
import {Profil} from '../../model/profil';

@Component({
  selector: 'app-match-list',
  templateUrl: './match-list.component.html',
  styleUrls: ['./match-list.component.css']
})
export class MatchListComponent implements OnInit {

  @Input() matchs: Match[] = [];
  @Output() matchDeleted = new EventEmitter();
  profils: Profil[] = [];

  constructor(private zinderService: ZinderService) {
  }

  ngOnInit() {
    this.zinderService.getProfils().subscribe(profils => {
      this.profils = profils;
    });
  }

  getProfil(id: string): Profil {
    return this.profils.find(p => p.id === id);
  }

  deleteMatch(id: string) {
    this.matchDeleted.emit(id);
  }
}
