import {Component, Input, OnInit} from '@angular/core';
import {Profil} from '../../model/profil';
import {ZinderService} from '../../service/zinder.service';
import {Interet} from '../../model/interet';

@Component({
  selector: 'app-profil-card',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  @Input() profil: Profil;
  @Input() interets: Interet[];

  constructor(private zinderService: ZinderService) {
  }

  ngOnInit() {
  }

  match(match: boolean) {
    this.zinderService.createMatch(this.profil.id, match).subscribe();
  }

  getInteretNom(interet: string): string {
    return this.interets ? this.interets.find(i => i.id === interet).nom : '';
  }
}
