import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ProfilComponent} from './component/profil/profil.component';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {StatsComponent} from './component/stats/stats.component';
import {ChartsModule} from 'ng2-charts';
import {PieChartComponent} from './component/pie-chart/pie-chart.component';
import {MatchPageComponent} from './component/match-page/match-page.component';
import {ZinderService} from './service/zinder.service';
import {MatchListComponent} from './component/match-list/match-list.component';
import {AdminPageComponent} from './component/admin-page/admin-page.component';

const appRoutes: Routes = [
  {path: '', component: MatchPageComponent},
  {path: 'stats', component: StatsComponent},
  {path: 'admin', component: AdminPageComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    ProfilComponent,
    StatsComponent,
    PieChartComponent,
    MatchPageComponent,
    MatchListComponent,
    AdminPageComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes
    ),
    FormsModule,
    ChartsModule
  ],
  providers: [
    ZinderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
