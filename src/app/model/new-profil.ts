export class NewProfil {
  nom: string;
  prenom: string;
  photoUrl: string;
  interets: string[];
}
