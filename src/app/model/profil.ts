export class Profil {
  photoUrl: string;
  id: string;
  prenom: string;
  nom: string;
  interets: string[];
}
