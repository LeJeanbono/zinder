import {Profil} from './profil';

export class Profils {
  profils: Profil[];
  nbProfils: number;
}
