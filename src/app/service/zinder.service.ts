import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Interet} from '../model/interet';
import {Profil} from '../model/profil';
import {map} from 'rxjs/operators';
import {Profils} from '../model/profils';
import {Match} from '../model/match';
import {NewProfil} from '../model/new-profil';

@Injectable()
export class ZinderService {

  private url = 'http://localhost:8088/zinder';

  constructor(private http: HttpClient) {
  }

  getInterets(): Observable<Interet[]> {
    return this.http.get<Interet[]>(`${this.url}/interets`);
  }

  createProfil(body: NewProfil): Observable<void> {
    return this.http.post<void>(`${this.url}/profils`, body);
  }

  getProfils(): Observable<Profil[]> {
    return this.http.get<Profils>(`${this.url}/profils`).pipe(
      map(profils => profils.profils)
    );
  }

  createMatch(idProfil: string, match: boolean): Observable<void> {
    const body = {match};
    return this.http.post<void>(`${this.url}/profils/${idProfil}/match`, body);
  }

  getMatchs(): Observable<Match[]> {
    return this.http.get<Match[]>(`${this.url}/matchs`);
  }

  deleteMatch(id: string): Observable<void> {
    return this.http.delete<void>(`${this.url}/matchs/${id}`);
  }
}
